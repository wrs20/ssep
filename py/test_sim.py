
from sim import Sim
import sys, os

S=10**7
n=60
l=120

s1 = Sim(n=n, l=l)
print(s1.state)


for qx in range(S):
    s1.step()
    if qx % 4 == 0:
        print(s1, end='\r', flush=True)
print(s1)
print(s1.state)
