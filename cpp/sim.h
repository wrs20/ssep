
#include "defines.h"
#include <vector>
#include <iostream>
#include <random>
#include <cmath>

class Sim {

  public:
    Sim(INT32 n, INT32 l, REAL rate);
    void print_locations();
    REAL get_expo_sample();
    INT32 get_jump_dir();

  private:
    void _set_sim_time(REAL time);
    INT32 _get_new_ind(INT32 ind);

    std::random_device _seed_rng{};
    std::mt19937 _mt_rng{_seed_rng()};

    std::uniform_real_distribution<REAL> _unif{0.0, 1.0};
    std::binomial_distribution<INT32> _binom{1,0.5};

    std::vector<INT32> _state;
    std::vector<REAL> _state_times;
    INT32 _n;
    INT32 _l;
    INT32 _rate;
    REAL _time;
};




