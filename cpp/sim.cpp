#include "sim.h"

using namespace std;

Sim::Sim(
    INT32 n, 
    INT32 l, 
    REAL rate
){
    _n = n;
    _l = l;
    _rate = rate;
    _state.reserve(_n);
    _state_times.reserve(_n);
    for(INT32 ix=0 ; ix<_n ; ix++){
        _state[ix] = ix;
        _state_times[ix] = 0.0;
    }
    _time = 0.0;
    

}

void Sim::print_locations(){
    for(INT32 ix=0 ; ix<_n ; ix++){
        cout << _state[ix] << "\t";
    }
    cout << endl;
}

REAL Sim::get_expo_sample(){
    return -1.0 * log(this->_unif(this->_mt_rng));
}

INT32 Sim::get_jump_dir(){
    return 1 - 2*(this->_binom(this->_mt_rng));
}

void Sim::_set_sim_time(REAL time){
    _time = time;
}

INT32 Sim::_get_new_ind(INT32 ind){
    const INT32 n = this->_n;
    const INT32 l = this->_l;

    const INT32 d = this->get_jump_dir();
    const INT32 p = this->_state[ind];
    const INT32 pp = (p + d + l) % l;

    if        ( (d<0) && (pp == this->_state[(ind-1+n)%n]) ){
        return p;
    } else if ( (d>0) && (pp == this->_state[(ind+1+n)%n]) ){
        return p;
    } else {
        return pp;
    }
}
