"""
Class to contain a simulation
"""

import numpy as np
from queue import PriorityQueue

import ctypes
REAL = ctypes.c_double
INT32 = ctypes.c_int32

class Sim(object):
    def __init__(self, n, l, rate=1.0):
        """
        n particles in simulation length l
        """
        self.n = n
        self.l = l
        self.rate = rate
        self.state = np.array(np.arange(n), dtype=INT32)
        self.state_times = np.zeros(self.n, dtype=REAL)
        self.q = PriorityQueue()
        self._init_state()
        self.time = 0.0

    def step(self):
        """
        Find the next moving particle, move the particle and
        recompute the next move time.
        """

        if self.q.empty():
            return

        t = self.q.get()
        tt = t[0]
        self._set_sim_time(tt)
        ind = t[1]
        indp = self._get_new_ind(ind)
        self.state[ind] = indp
        self._set_next_move(ind)

    def _get_new_ind(self, ind):
        d = self._get_jump_dir()
        p = self.state[ind]
        pp = (p + d) % self.l
        if   (d < 0) and pp == self.state[ind-1]:
            return p
        elif (d > 0) and pp == self.state[(ind + 1) % self.n]:
            return p
        else:
            return pp

    def _get_jump_dir(self):
        return 1 - 2 * np.random.binomial(1,0.5,1)[0]

    def _set_sim_time(self, t):
        self.time = t
    
    def _set_next_move(self, ind):
        time = self.time + self._get_exp_sample()
        self.state_times[ind] = time
        self.q.put((time, ind))
    
    def _get_exp_sample(self):
        return np.random.exponential(scale=self.rate)
    
    def _init_state(self):
        for ix in range(self.n):
            time = self._get_exp_sample()
            self.q.put((time, ix))

    def __repr__(self):
        s = np.chararray(self.l)
        s[:] = '.'
        for px in range(self.n):
            s[self.state[px]] = '|'
        return s.tostring().decode("utf-8")
    



