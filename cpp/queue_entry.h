
#include "defines.h"


class QueueEntry {

  public:
    QueueEntry(REAL t, INT32 i)
    {
        _t = t;
        _i = i;
    }
    bool operator< (QueueEntry &other);

    REAL time(){ return this->_t; };
  private:
    REAL _t;
    INT32 _i;
};


